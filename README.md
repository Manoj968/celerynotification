## Celery Documentation
https://docs.celeryproject.org/en/stable/

## github link
https://github.com/priyanshu2015?tab=repositories

https://github.com/priyanshu2015/celery-with-django/blob/main/django_celery_project/settings.py

## celery.schedules
https://docs.celeryproject.org/en/stable/reference/celery.schedules.html

## Install Celery
pip install celery
pip install django-celery-results
pip install django-celery-beat

## Install Channels
python -m pip install -U channels
## or
pip install channels

## Install Channel-Redis
pip install channels-redis

## Install Asyncio
pip install asyncio

# install redis
pip install redis

## Install Redis Server in Ubuntu
sudo apt install redis-server

## restart the Redis service
sudo systemctl restart redis.service

## starting redis server
redis-server

## checking that the Redis service is running
sudo systemctl status redis

## check server is working on cli
redis-cli
>ping #ans is Pong

## to exit from cli
exit

redis-cli ping

## More about Redis server
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04

## if you uing window redis server will not support for windows in case use redis lab
login -> https://app.redislabs.com/#/login after login you create database and import is endpoints for communication

signup -> https://redis.com/try-free/

## How TO Run Celery
At first we need to run redis using redis-server command than we need to run this command
## For Celery worker
celery -A celeryNotification worker -l INFO
## For Celery worker Beat
celery -A celeryNotification beat -l INFO